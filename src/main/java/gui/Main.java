package gui;

import cellular.CellAutomaton;
import cellular.GameOfLife;
import cellular.BriansBrain;
/** import cellular.BrainsBrain;
 */

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new BriansBrain(100,100);
		//CellAutomaton ca = new BriansBrain(100, 100);
		CellAutomataGUI.run(ca);
	}

}
