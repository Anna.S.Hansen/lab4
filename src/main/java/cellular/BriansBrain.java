package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton {


    int rows;
	int columns;
	IGrid currentGeneration;
    
    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		this.rows = rows;
		this.columns = columns;
		initializeCells();
    }

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.rows;
	}

	@Override
	public int numberOfColumns() {
		return this.columns;
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState state = getCellState(row, col);
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
        if (state.equals(CellState.ALIVE)){
            state = CellState.DYING;
        }
		else if (state.equals(CellState.DYING)){
			state = CellState.DEAD;
		} 
        else if (state.equals(CellState.DEAD) && aliveNeighbors == 2) {
            state = CellState.ALIVE;
        }
		return state;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for (int i = row-1; i <= row + 1; i++){
			for (int j = col-1; j <= col+1; j++){
				try {
                    if (getCellState(i,j).equals(state) && !(i==row && j==col)){
                        count++;
                    }
                    } catch (Exception ignored) {}		
			}
		}
		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
