package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows; 
    private int columns;
    private CellState initialState;
    CellState[][] grid = new CellState[rows][columns];
    


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
            }
        }
        
        

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return grid.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row >= 0 & row <= numRows() & column >= 0 & column <= numColumns()) {
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row >= 0 & row <= numRows() & column >= 0 & column <= numColumns()) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(rows, columns, initialState);
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                newGrid.set(i, j, grid[i][j]);
            }
        }
        return newGrid;   
    }
    
}
